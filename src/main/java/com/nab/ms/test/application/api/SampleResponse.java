package com.nab.ms.test.application.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SampleResponse {
    private String message;
}
