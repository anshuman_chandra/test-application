package com.nab.ms.test.application.application.FlightDetails;

import com.nab.ms.test.application.repository.downstream.dto.DownstreamRequest;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class FlightDetailsRequestMapper {

    public DownstreamRequest mapRequest(@NotNull String flightNumber,
                                        @NotNull String arrival,
                                        @NotNull String destination) {
        return DownstreamRequest.builder()
                .flightNumber(flightNumber)
                .arrival(arrival)
                .destination(destination)
                .build();
    }
}
