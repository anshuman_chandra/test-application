package com.nab.ms.test.application.application.FlightDetails;

import com.nab.ms.test.application.repository.downstream.DownstreamRepository;
import com.nab.ms.test.application.repository.downstream.dto.DownstreamRequest;
import com.nab.ms.test.application.repository.downstream.dto.DownstreamResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.naming.ServiceUnavailableException;

@Service
@RequiredArgsConstructor
public class FlightDetailsService {
    private final DownstreamRepository downstreamRepository;
    private final FlightDetailsRequestMapper flightDetailsRequestMapper;

    public Mono<DownstreamResponse> fetchFlightConfirmation(String flightNumber, String arrival, String destination) {
        DownstreamRequest downstreamRequest = flightDetailsRequestMapper.mapRequest(flightNumber, arrival, destination);
        return Mono.just(downstreamRequest)
                .flatMap(downstreamRepository::requestFlightDetails)
                .onErrorMap(err -> new ServiceUnavailableException("Exception Thrown"));
    }
}
