package com.nab.ms.test.application.application.Sample;

import com.nab.ms.test.application.api.SampleResponse;
import com.nab.ms.test.application.repository.sample.SampleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SampleService {
    private final SampleRepository sampleRepository;

    public SampleResponse getHelloWorld() {
        return sampleRepository.getHelloWorld();
    }
}
