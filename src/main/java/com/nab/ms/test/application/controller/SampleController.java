package com.nab.ms.test.application.controller;

import com.nab.ms.test.application.api.SampleResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.netty.http.server.HttpServerRequest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class SampleController {
    @PostMapping("/v1/testapplication/helloworld/users/{userId}")
    public ResponseEntity<SampleResponse> helloWorld(
            @PathVariable final int version, @PathVariable final int userId,
            @RequestHeader final HttpHeaders requestHeaders,
            final HttpServerRequest request) {
        return new ResponseEntity(new SampleResponse(), HttpStatus.OK);

    }
}
