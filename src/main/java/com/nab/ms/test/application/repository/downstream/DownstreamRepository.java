package com.nab.ms.test.application.repository.downstream;

import com.nab.ms.test.application.repository.downstream.dto.DownstreamRequest;
import com.nab.ms.test.application.repository.downstream.dto.DownstreamResponse;
import com.nab.ms.test.application.validation.BeanValidationHelper;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixException;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
@Slf4j
@DefaultProperties(raiseHystrixExceptions = {HystrixException.RUNTIME_EXCEPTION})
public class DownstreamRepository {

    @Value("${infrastructure.services.downstream}")
    private String downstreamEndpoint;

    private final DownstreamErrorMapper downstreamErrorMapper;

    private final WebClient webClient;
    private final BeanValidationHelper beanValidationHelper;

    public Mono<DownstreamResponse> requestFlightDetails(@NotNull final DownstreamRequest request) {
        return HystrixCommands
                .from(fetchFlightDetils(request))
                .commandName("requestFlightDetails")
                .toMono();
    }

    private Mono<DownstreamResponse> fetchFlightDetils(
            @NotNull final DownstreamRequest request) {
        return webClient.post()
                .uri(downstreamEndpoint)
                .bodyValue(request)
                .header(new HttpHeaders().toString())
                .retrieve()
                .onStatus(HttpStatus::isError, downstreamErrorMapper::mapError)
                .onStatus(httpStatus -> httpStatus != HttpStatus.OK, downstreamErrorMapper::mapUnknownStatus)
                .bodyToMono(DownstreamResponse.class)
                .doOnNext(downstreamResponse -> downstreamResponse.setStatus(HttpStatus.OK))
                .map(beanValidationHelper::applyValidations)
                .onErrorMap(ConstraintViolationException.class, downstreamErrorMapper::mapValidationError);
    }
}
