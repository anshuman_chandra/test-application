package com.nab.ms.test.application.repository.downstream.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@Getter
@AllArgsConstructor
public class DownstreamRequest {
    private String flightNumber;
    private String arrival;
    private String destination;
}
