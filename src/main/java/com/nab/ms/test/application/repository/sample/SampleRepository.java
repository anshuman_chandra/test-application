package com.nab.ms.test.application.repository.sample;

import com.nab.ms.test.application.api.SampleResponse;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SampleRepository {

    @HystrixCommand(commandKey = "smapleCommand")
    public SampleResponse getHelloWorld() {
        long startTime = System.currentTimeMillis();
        try {
            return new SampleResponse("Hello World");
        } finally {
            log.info("Hello World Invoked - " +startTime);
        }
    }
}
