package com.nab.ms.test.application.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@AllArgsConstructor
public class BeanValidationHelper {

    private final Validator validator;

    public <R> R applyValidations(R object) {
        Set<ConstraintViolation<R>> violations = validator.validate(object);

        if(CollectionUtils.isNotEmpty(violations)) {
            throw new ConstraintViolationException(violations);
        } else {
            return object;
        }
    }
}
