package component.com.nab.ms.test.application.controller;

import com.nab.ms.test.application.MsTestApplication;
import com.netflix.hystrix.Hystrix;
import com.netflix.hystrix.metric.consumer.HealthCountsStream;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.cloud.contract.spec.internal.MediaTypes.APPLICATION_JSON;

@TestComponent
@SpringBootTest(classes = MsTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SampleControllerTest {

    private static final String API_PATH = "/v1/testapplication/helloworld/users/1";

    @LocalServerPort
    private int serverPort;

    @BeforeEach
    void setup() {
        RestAssured.port = serverPort;
        RestAssured.basePath = "";

        Hystrix.reset();
        HealthCountsStream.reset();
    }

    @Test
    void testMainApplication() {
        //formatter:off
        //given()/*
         //    .contentType(APPLICATION_JSON)*/
    }
}
